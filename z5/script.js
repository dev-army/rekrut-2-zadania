

window.onload = function(e) {

  function showData() {
    var name = document.querySelector('input[name=name]').value;
    var surname = document.querySelector('input[name=surname]').value;

    var newP = document.createElement("p");
    newP.appendChild(document.createTextNode(name + ' ' + surname));

    document.querySelector('body').appendChild(newP);
  }

  function onSubmit(event) {
    event.preventDefault();
    var result = window.confirm("sometext");

    if (result) {
      showData();

      document.querySelector('header').remove();
      document.querySelector('main').remove();
    }


  }

  const form = document.querySelector('form');
  form.addEventListener('submit', onSubmit);

}